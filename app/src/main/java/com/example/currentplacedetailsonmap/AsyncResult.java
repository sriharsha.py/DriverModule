package com.example.currentplacedetailsonmap;

import org.json.JSONObject;


interface AsyncResult
{
    void onResult(JSONObject object);
}